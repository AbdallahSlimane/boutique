<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Commande;
use Doctrine\Common\DataFixtures\DependentFixtureInterface as DFI;

class CommandeFixtures extends BaseFixture implements DFI
{
    public function loadData(ObjectManager $manager)
    {
        
        $this->createMany(20, "commande", function($num){
            
            $commande = new Commande;
            $commande->setMontant($this->faker->randomFloat(2,100,500));
            $commande->setDateEnregistrement($this->faker->dateTime('now'));
            $commande->setEtat($this->faker->randomElement(["en cours","en attente","livrée"]));
            $commande->setMembre($this->getRandomReference("membre"));
            return $commande;
        });

        $manager->flush();
    }
    public function getDependencies(){
        // J'indique les fixtures qui doivent être lancées avant la fixture actuelle.
        return [ MembreFixtures::class ];
    }
}
